package configo

import (
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	Listen struct {
		Ip   string `json: "ip"`
		Port string `json: "port"`
	}
	Log    string `json: "log"`
	TplDir string `json: "tpldir"`
	Db     struct {
		Type     string
		Host     string
		User     string
		Password string
		Dbname   string
		Sslmode  string
	}
}

func Read(params ...string) (conf *Configuration, err error) {
	file_name := "conf/config.json"
	if len(params) > 0 {
		file_name = params[0]
	}
	bytes, err := ioutil.ReadFile(file_name)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bytes, &conf)
	if err != nil {
		return nil, err
	}
	return conf, nil
}
