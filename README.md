# package configo
```json
{
    "listen": {
        "ip": "127.0.0.1",
        "port": "8080"
    },
    "log": "/var/log/test/test.log",
    "db": {
        "type": "postgres",
        "host": "127.0.0.1",
        "user": "postgres",
        "password": "password",
        "dbname": "test",
        "sslmode": "disable"
    }
}
```

```go
package main

import (
	"fmt"

	"bitbucket.org/sergsoloviev/configo"
)

func main() {
	//conf, err := configo.Read("conf/config.json")
	conf, err := configo.Read()
	if err != nil {
		panic(err)
	}
	fmt.Println(conf)
}
```
